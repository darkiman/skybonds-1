const PERCENTS = 100;
const DECIMALS_MULTIPLIER = 1000;
const DECIMALS_PART_DEFAULT = '000';

const calculatePercent = input => {
	const total = input.reduce((acc, val) => acc + parseFloat(val), 0);
	return input.map(num => {
			let [intPart, decimalPart] = (Math.round((num * DECIMALS_MULTIPLIER * PERCENTS) / total) / DECIMALS_MULTIPLIER).toString().split('.');
			if (isNaN(intPart)) {
				intPart = 0;
			}
			const decimals = decimalPart ? `${decimalPart}${DECIMALS_PART_DEFAULT.slice(decimalPart.length)}` : DECIMALS_PART_DEFAULT;
			return `${intPart}.${decimals}`
		}
	)
};

module.exports = {
	calculatePercent
};
