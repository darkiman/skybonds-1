const chai = require('chai');
const { calculatePercent } = require('./calculatePercent');

const testData = [
	{
		input: ['1.5', '3', '6', '1.5'],
		output: `["12.500","25.000","50.000","12.500"]`,
	},
	{
		input: ['2', '2', '2', '2'],
		output: `["25.000","25.000","25.000","25.000"]`,
	},
	{
		input: ['0', '0', '0', '0'],
		output: `["0.000","0.000","0.000","0.000"]`,
	},
	{
		input: ['2', '4', '6'],
		output: `["16.667","33.333","50.000"]`,
	},
	{
		input: ['2'],
		output: `["100.000"]`,
	},
	{
		input: ['0'],
		output: `["0.000"]`,
	},
	{
		input: [],
		output: `[]`,
	},
];

describe('calculatePercent', () => {
		it('test data', () => {
			testData.map(item => {
				chai.assert.equal(item.output, JSON.stringify(calculatePercent(item.input)))
			})
		})
	}
);
